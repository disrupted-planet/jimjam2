package com.jimjam.engine.gfx;

public class Font {
	
	public static final Font STANDARD = new Font("/sprite.png");
	
	
	private Image fontImage;
	private int[] offsets;
	private int[] widths;
	
	public Font(String path) {
		fontImage = new Image(path);
		
		offsets = new int[58];
		widths = new int[58];
		
		int unicode = 0;
		
		for (int i = 0; i < fontImage.getW(); i++) {
			if (fontImage.getP()[i] == 0xff_00_00_ff) {
				offsets[unicode] = i;
			}
			if (fontImage.getP()[i] == 0xff_ff_ff_00) {
				widths[unicode] = i - offsets[unicode];
			}
		}
	}

	public Image getFontImage() {
		return fontImage;
	}

	public void setFontImage(Image fontImage) {
		this.fontImage = fontImage;
	}

	public int[] getOffsets() {
		return offsets;
	}

	public void setOffsets(int[] offsets) {
		this.offsets = offsets;
	}

	public int[] getWidths() {
		return widths;
	}

	public void setWidths(int[] widths) {
		this.widths = widths;
	}
}
