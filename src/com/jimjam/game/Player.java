package com.jimjam.game;

import com.jimjam.engine.gfx.Image;

public class Player {
	
	private int posX;
	private int posY;
	private int width = 84;
	private int height = 184;
	private boolean isAlive;
	
	private int g = 1;
	private int v0 = 20;
	
	private int speedY = v0;
	
	private boolean jumping = false;
	
	private Image[] walkRightSprites;
	private Image[] walkLeftSprites;
	
	private Image[] jumpRightSprites;
	private Image[] jumpLeftSprites;
	
	
	private Image faceSprite;
	
	public Player (int x, int y) {
		posX = x;
		posY = y;
		isAlive = true;
		
		faceSprite = new Image("/jim_face.png");
		
		walkLeftSprites = new Image[3];
		walkLeftSprites[0] = new Image("/jim_marche_left1.png");
		walkLeftSprites[1] = new Image("/jim_marche_left2.png");
		walkLeftSprites[2] = new Image("/jim_marche_left3.png");
		
		walkRightSprites = new Image[3];
		walkRightSprites[0] = new Image("/jim_marche_right1.png");
		walkRightSprites[1] = new Image("/jim_marche_right2.png");
		walkRightSprites[2] = new Image("/jim_marche_right3.png");
		
		jumpLeftSprites = new Image[3];
		jumpLeftSprites[0] = new Image("/jim_saute_left1.png");
		jumpLeftSprites[1] = new Image("/jim_saute_left2.png");
		jumpLeftSprites[2] = new Image("/jim_saute_left3.png");
		
		jumpRightSprites = new Image[3];
		jumpRightSprites[0] = new Image("/jim_saute_right1.png");
		jumpRightSprites[1] = new Image("/jim_saute_right2.png");
		jumpRightSprites[2] = new Image("/jim_saute_right3.png");
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getPosX() {
		return posX;
	}
	
	public int getPosY() {
		return posY;
	}
	
	public void setPosX(int newX) {
		posX = newX;
	}
	
	public void increaseX(int add) {
		posX += add;
	}
	
	public void increaseY(int add) {
		posY += add;
	}

	public Image[] getWalkRightSprites() {
		return walkRightSprites;
	}

	public Image getFaceSprite() {
		return faceSprite;
	}

	public Image[] getWalkLeftSprites() {
		return walkLeftSprites;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public int getG() {
		return g;
	}

	public int getV0() {
		return v0;
	}

	public boolean isJumping() {
		return jumping;
	}

	public void setJumping(boolean jumping) {
		this.jumping = jumping;
		if (jumping) {
			speedY = v0;
		} else {
			speedY = 0;
		}
	}

	public Image[] getJumpRightSprites() {
		return jumpRightSprites;
	}

	public Image[] getJumpLeftSprites() {
		return jumpLeftSprites;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
}
