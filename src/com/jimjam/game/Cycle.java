package com.jimjam.game;

import java.awt.Polygon;

public class Cycle {

	private int width;
	private int height;
	private int gameWidth;
	
	public int ns_top_x;
	public int ns_bot_x;
	public int ns_top_y;
	public int ns_bot_y;
	
	public int ne_top_x;
	public int ne_bot_x;
	public int ne_top_y;
	public int ne_bot_y;
	
	public Cycle(int width, int height, int gameWidth) {
		this.width = width;
		this.height = height;
		this.gameWidth = gameWidth;
		
		ns_top_x = 0;
		ns_top_y = 0;
		
		ns_bot_x = ns_top_x + 300;
		ns_bot_y = height;
		
		ne_top_x = gameWidth / 4;
		ne_top_y = 0;
		
		ne_bot_x = ne_top_x - 300;
		ne_bot_y = height;
	}

	public void advance(int delta) {
		ns_top_x += delta;
		ne_bot_x += delta;
		
		if (ns_top_x > gameWidth) ns_top_x = 0;
		if (ne_bot_x > gameWidth) ne_bot_x = 0;

		ns_bot_x = ns_top_x + 300;
		ne_top_x = ne_bot_x + 300;
		
	}
	
	public Polygon getPolygon1(int actualX) {
		int botX = actualX + 300;
		Polygon p = new Polygon(new int[] {actualX - width, botX - width,   botX, actualX},
								new int[] {0, height, height, 0}, 4);
		return p;
	}
	
	public Polygon getPolygon2(int actualX) {
		int topX = actualX + 300;
		Polygon p = new Polygon(new int[] {actualX,  actualX + width, topX + width, topX},
								new int[] { height, height,     0, 0}, 4);	
		return p;
	}

}
