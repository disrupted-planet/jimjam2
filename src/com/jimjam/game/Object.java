package com.jimjam.game;

import com.jimjam.engine.gfx.Image;

public class Object {
    private int id;
    private String name;
    private Image image;
    private Image bigImage;
    private boolean usable;
    private int posX;
    private int posY;
    
    public Object (int id, String name, Image image, Image bigImage, int x, int y, boolean usable) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.bigImage = bigImage;
        this.posX = x;
        this.posY = y;
        this.usable = usable;
    }
    
    public String getname() {
        return name;
    }
    
    public Image getImage() {
        return image;
    }
   
    public Image getBigImage() {
        return bigImage;
    }
    
    public int getX() {
        return posX;
    }
    
    public int getY() {
        return posY;
    }
    
    public int getId() {
        return id;
    }
    
    public boolean getUsable() {
    	return usable;
    }
}