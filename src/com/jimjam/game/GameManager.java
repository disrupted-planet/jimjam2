package com.jimjam.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;

import com.jimjam.engine.AbstractGame;
import com.jimjam.engine.GameContainer;
import com.jimjam.engine.Renderer;
import com.jimjam.engine.audio.SoundClip;
import com.jimjam.engine.gfx.Image;

public class GameManager extends AbstractGame {
	
	// private Image image;
	private String[] printMap = {
			"                                                      0000000         000000               0                                             ",	// 10
			"                                                    00      10000    0111  100         00001                                             ",	//  9
			"                                                   0           11000011      10000000001                                                 ",	//  8
			"                                                  0                                                                                      ",	//  7
			"                                                                                                                                         ",	//  7
			"                                                                                                                                         ",	//  7
			"                                                                                                                                         ",	//  6
			"444444                                                                                                                                  4",	//  5
			"55555544444                                   0                                                             22                       4445",	//  4
			"555555555554444                  44444444444441666666666666666666666666666666666666662666666662222222222223322     222           44445555",	//  3
			"55555555555555554444444444444444455555555555551201110000000000000000000000000000000001122222221111111111133333222223332225544444455555555",	//  2
		 	"55555555555555555555555555555555555555555555555511111111111111111111111111111111111111111111111111111113333333333333333555555555555555555"	//  1
		};
	
	private Block[][] map = new Block[printMap.length][printMap[0].length()];
	private ArrayList<Object> objects = new ArrayList<Object>();
	
	private boolean start;
	private boolean end;
	
	private boolean gotGlass = false;
	private boolean openObjectives = false;
	
	private boolean healJam, fixDome;
	private boolean isAtWorkshop = false;
	
	private Image background = new Image("/arriere_plan.png");
	private Image gameOver = new Image("/game_over.png");
	
	private Image[] startAnim = new Image[] {
			new Image("/animation1.png"),
			new Image("/animation2.png"),
			new Image("/animation3.png"),
			new Image("/animation4.png"),
			new Image("/animation5.png"),
			new Image("/animation6.png"),
			new Image("/animation7.png"),
			new Image("/animation8.png"),
			new Image("/animation9.png"),
			new Image("/animation10.png"),
			new Image("/animation11.png"),
			new Image("/animation12.png"),
			new Image("/animation13.png"),
			new Image("/animation14.png"),
			new Image("/animation15.png"),
			new Image("/animation16.png"),
			new Image("/animation17.png"),
			new Image("/animation18.png")
	};
	
	private Image[] endAnim = new Image[] {
			new Image("/animation_fin0.png"),
			new Image("/animation_fin1.png"),
			new Image("/animation_fin2.png"),
			new Image("/animation_fin3.png")
	};
	
	private int width = 1366, height = 768;
	private int squareSize = 40;
	private int screenX;
	
	private int[] completeObjectives = {950, 250, 950+40, 250+40};
	private int[] workshopCoordonate = {4920,114,4920+200,200+80};
	private int[] jamCoordonate = {800,height,800+973,0};
	
	private Player player;
	private Cycle cycle;
	private Inventory inventory = new Inventory();
	
	private SoundClip music;
	
	private Image stateOne = new Image("/jam_background_verre_blessure.png");
	private Image stateTwo = new Image("/jam_background_verre_pansement.png");
	private Image stateThree = new Image("/jam_background_blessure.png");
	private Image stateFour = new Image("/jam_backgroundrepare.png");
	
	private float temp = 0;
	
	public GameManager() {
		music = new SoundClip("/musicjimjam.wav");
		Block.init();
		
		// image = new Image("/sprite.png");
		player = new Player(900,80);
		cycle = new Cycle(width, height, squareSize * map[0].length);
		for(int y = 0; y < printMap.length; y ++) {
			for(int x = 0; x < printMap[0].length(); x ++) {
				map[y][x] = new Block(printMap[y].charAt(x));
			}
		}
		screenX = player.getPosX()-width/2+50;
		if (screenX<0) screenX = map[0].length*squareSize+screenX;
		
		start = true;
		end = false;
		healJam = false;
		fixDome = false;
		
        objects.add(new Object(1, "Coconut",new Image("/cacahuete_vide.png"),new Image("/cacahuete_vide72.png"),1840,200, true));
        objects.add(new Object(2, "Clay",new Image("/argile.png"),new Image("/argile72.png"),2500,120, true));
        objects.add(new Object(3, "Coal",new Image("/charbon.png"),new Image("/charbon72.png"),3280,440, false));
        objects.add(new Object(4, "Rock",new Image("/pierre.png"),new Image("/pierre72.png"),4000,160, false));
        // Eau = 6
        // Sable = 7
        // Four = 8
        // Verre = 9
        
	}

	@Override
	public void update(GameContainer gc, float dt) {
		if (start) {
			temp = (temp + dt * 3);
			
			if (temp >= 18) {start = false; temp = 0;}
			else return;
		}
		
		if (end) {
			temp = (temp + dt * 2);
			if (temp >= 4) temp = 4;
			return;
		}
		
		
		if (!player.isAlive()) return;
		
		// Open Objectives 
        if (gc.getInput().isKeyDown(KeyEvent.VK_E)) openObjectives = !openObjectives;
		
    	int[] playerBox = {player.getPosX(),player.getPosY()+player.getHeight(),player.getPosX()+player.getWidth(),player.getPosY()};
    	int[] workShop = {workshopCoordonate[0], workshopCoordonate[3], workshopCoordonate[2], workshopCoordonate[1]};
    	if(collision(playerBox, workShop)) isAtWorkshop = true;
    	else isAtWorkshop = false;	
		
		if (!music.isRunning()) music.loop();
		
		// Open Inventory
        if (gc.getInput().isKeyDown(KeyEvent.VK_A)) {
            if (inventory.isOpen()) inventory.setOpen(false);
            else inventory.init();
        }
        
        // Pick Item
        if(gc.getInput().isKeyDown(KeyEvent.VK_Z)) {
            Object groundObject = popObject();
            if(groundObject != null) {
                inventory.addObject(groundObject);
            }
        }
        
        // Move into inventory
        if(inventory.isOpen()) {
            if(inventory.getSelected()){
                if(gc.getInput().isKeyDown(49)) {
                	if(inventory.getObjectAt(inventory.getSelectedIndex()).getUsable()) {
                		boolean drop = false;
                		if (inventory.getObjectAt(inventory.getSelectedIndex()).getId() == 1){
                			ArrayList<Integer> blocks = new ArrayList<Integer>();
                	        int xIndexStart = (int) Math.floor((player.getPosX()-5)/squareSize);
                	        int xIndexEnd = xIndexStart + (int) Math.floor(player.getWidth()/squareSize) + 2;
                	        
                	        for(int y = 0; y < printMap.length; y ++) {
                	            for(int x = xIndexStart; x<=xIndexEnd; x++) {
                	            	blocks.add(Character.getNumericValue(map[y][x].getType()));
                	            }
                	        }
                			inventory.useItem(blocks);
                		} else if (inventory.getObjectAt(inventory.getSelectedIndex()).getId() == 9){
                			if(collision(playerBox,jamCoordonate)) {
                				drop = true;
                				fixDome = true;
                				if(healJam) stateOne = stateFour;
                				else stateOne = stateThree;
                			}
                		} else if (inventory.getObjectAt(inventory.getSelectedIndex()).getId() == 2){
                			if(collision(playerBox,jamCoordonate)) {
                				drop = true;
                				healJam = true;
                				if(fixDome) stateOne = stateFour;
                				else stateOne = stateTwo;
                			}                			
                		} 
                		if(drop) {
                			inventory.dropObject(inventory.getSelectedIndex());
                			inventory.unselect();
                		}
                		
                	}
                    
                } else if (gc.getInput().isKeyDown(50)) {
                    if(inventory.isInCrafts(inventory.getObjectAt(inventory.getSelectedIndex()).getId())) {
                        inventory.deleteCraftItem(inventory.getObjectAt(inventory.getSelectedIndex()).getId());
                    } else {
                        if(inventory.craftEmpty()) inventory.putInCraft(inventory.getSelectedIndex());
                        inventory.unselect();
                    }
                } else if (gc.getInput().isKeyDown(51)) {
                    inventory.unselect();
                }
            } else {
            	if(isAtWorkshop) {
            		if(gc.getInput().isKeyDown(KeyEvent.VK_C)) {
	                    inventory.letsCraft(0);
	                    gotGlass = true;
	                }
            	}

                
                for(int i = 49; i<58;i++) {
                    if(gc.getInput().isKeyDown(i)) {
                        if(inventory.getObjectAt(i-49)!=null) {
                            inventory.select(i-49);
                        }
                        
                    }
                }
            }
        }
		
		// Move LEFT
		if (gc.getInput().isKey(KeyEvent.VK_LEFT)){
			if (canMove(-5,0,-5,0)) {
				if(player.getPosX()-5<0) {
					player.setPosX(map[0].length*squareSize);
				} else {
					player.increaseX(-5);
					screenX -= 5;
					if (screenX<0) screenX = map[0].length*squareSize+screenX;
				}
			}
		}
		
		// Move RIGHT
		if (gc.getInput().isKey(KeyEvent.VK_RIGHT)){
			if(canMove(5,0,5,0)) {
				if(player.getPosX()+5>map[0].length*squareSize) {
					player.setPosX(0);
				} else {
					player.increaseX(5);
					screenX += 5;
					if (screenX>= map[0].length*squareSize) screenX = screenX - map[0].length*squareSize;
				}
			}
		}

		//Move BOTTOM
		if(canMove(0,-player.getG(),0,-player.getG()) && !player.isJumping()) {
			player.setJumping(true);
			player.setSpeedY(0);
			player.increaseY(player.getSpeedY());
		}
		
		//Move UP
		if(gc.getInput().isKey(KeyEvent.VK_UP) && !player.isJumping()){
//			if(canMove(0,5,0,5)) {
//				player.increaseY(10);
//			}
			player.setJumping(true);
		}
		
		if (player.isJumping()) {
			if (canMove(0, player.getSpeedY(), 0, player.getSpeedY())) {
				player.increaseY(player.getSpeedY());
			} else {
				player.setJumping(false);
				player.setPosY(Math.round(player.getPosY() / squareSize) * squareSize);
			}
			player.setSpeedY(player.getSpeedY() - player.getG());
		}
		
		cycle.advance(3);
		
		Polygon p;
		if (screenX > map[0].length * squareSize - width && cycle.ns_top_x - screenX + map[0].length * squareSize < width) {
			p = cycle.getPolygon1(cycle.ns_top_x - screenX + map[0].length * squareSize);
		} else {
			p = cycle.getPolygon1(cycle.ns_top_x - screenX);
		}
		Polygon p2;
		if (screenX > map[0].length * squareSize - width && cycle.ne_bot_x - screenX + map[0].length * squareSize < width) {
			p2 = cycle.getPolygon2(cycle.ne_bot_x - screenX + map[0].length * squareSize);
		} else {
			p2 = cycle.getPolygon2(cycle.ne_bot_x - screenX);
		}

		if (p.contains(gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight(), player.getWidth(), player.getHeight())
		|| p2.contains(gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight(), player.getWidth(), player.getHeight())) {
			player.setAlive(false);
		}
		
		temp = (temp + dt * 5) % 3;
		
		if (healJam && fixDome) { end = true; temp = 0; }
	}

	@Override
	public void render(GameContainer gc, Renderer r) {
		if (start) {
			r.drawImage(startAnim[(int) temp], 0, 0);
			return;
		}
		
		if (end) {
			r.drawImage(endAnim[(int) temp], 0, 0);
			return;
		}
		
		if (!player.isAlive()) {
			r.drawImage(gameOver, 0, 0);
			return;
		}
		
		int index = (int) Math.floor(screenX/squareSize);
		if (index == map[0].length) index = 0;

		int startPoint = index*squareSize - screenX;

		Graphics g = gc.getWindow().getImage().getGraphics();
		
		r.drawImage(background, -screenX/2, 80);
		r.drawImage(background, -screenX/2+1366, 80);
		r.drawImage(background, -screenX/2+1366*2, 80);
		r.drawImage(background, -screenX/2+1366*3, 80);
		
		if(screenX > map[0].length*squareSize - width && player.getPosX()<width) r.drawImage(stateOne, 800-screenX+map[0].length*squareSize, 280);
		else r.drawImage(stateOne, 800-screenX, 280);
		
		while (startPoint < gc.getWidth()) {
			for(int i = 0; i < map.length; i++) {
				Image img = map[i][index].getImage();
                if (img != null) r.drawImage(img, startPoint, gc.getHeight()-squareSize*(map.length-(i+1))-squareSize);
				//g.fillRect(startPoint, squareSize*(map.length-(i+1)), squareSize, squareSize);
			}
			index ++;
			if (index == map[0].length) index = 0;
			startPoint += squareSize;
		}
		
		r.drawImage(new Image("/atelier.png"),workshopCoordonate[0]-screenX, gc.getHeight()-workshopCoordonate[3]);

		if (player.isJumping()) {
			if (gc.getInput().isKey(KeyEvent.VK_LEFT)) {
				if (player.getSpeedY() >= 0) r.drawImage(player.getJumpLeftSprites()[1],gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight());
				else r.drawImage(player.getJumpLeftSprites()[2],gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight());
			} else {
				if (player.getSpeedY() >= 0) r.drawImage(player.getJumpRightSprites()[1],gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight());
				else r.drawImage(player.getJumpRightSprites()[2],gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight());
				
			}
			
		} else if (gc.getInput().isKey(KeyEvent.VK_RIGHT)) {
			r.drawImage(player.getWalkRightSprites()[(int) temp],gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight());
		} else if (gc.getInput().isKey(KeyEvent.VK_LEFT)) {
			r.drawImage(player.getWalkLeftSprites()[(int) temp],gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight());
		} else {
			r.drawImage(player.getFaceSprite(),gc.getWidth()/2-50, gc.getHeight()-player.getPosY()-player.getHeight());
		}
		
		for (Iterator<Object> i = objects.iterator(); i.hasNext();) {
            Object item = i.next();
            int imgX = item.getX()-screenX;
            if(imgX>-squareSize && imgX<(1366+squareSize)) {
                r.drawImage(item.getImage(),imgX, gc.getHeight()-item.getY());
            }
        }
		
		g.setColor(new Color(50, 50, 50, 200));
		if (screenX > map[0].length * squareSize - width && cycle.ns_top_x - screenX + map[0].length * squareSize < width) {
			g.drawLine(cycle.ns_top_x - screenX + map[0].length * squareSize, cycle.ns_top_y, cycle.ns_bot_x - screenX + map[0].length * squareSize, cycle.ns_bot_y);
			g.fillPolygon(cycle.getPolygon1(cycle.ns_top_x - screenX + map[0].length * squareSize));
		} else {
			g.drawLine(cycle.ns_top_x - screenX, cycle.ns_top_y, cycle.ns_bot_x - screenX, cycle.ns_bot_y);
			g.fillPolygon(cycle.getPolygon1(cycle.ns_top_x - screenX));
		}
		if (screenX > map[0].length * squareSize - width && cycle.ne_bot_x - screenX + map[0].length * squareSize < width) {
			g.fillPolygon(cycle.getPolygon2(cycle.ne_bot_x - screenX + map[0].length * squareSize));
			g.drawLine(cycle.ne_top_x - screenX + map[0].length * squareSize, cycle.ne_top_y, cycle.ne_bot_x - screenX + map[0].length * squareSize, cycle.ne_bot_y);
		} else {
			g.fillPolygon(cycle.getPolygon2(cycle.ne_bot_x - screenX));
			g.drawLine(cycle.ne_top_x - screenX, cycle.ne_top_y, cycle.ne_bot_x - screenX, cycle.ne_bot_y);
		}
		
		/* TEST */
		g.setColor(new Color(255,0,0));
		g.fillRect(completeObjectives[0] - screenX, gc.getHeight()-completeObjectives[1]-completeObjectives[2], completeObjectives[2], completeObjectives[3]);
		
		
/* Draw Inventory */ 
		
		if (inventory.isOpen()) {
			g.setColor(new Color(180,180,180, 200));
			g.fillRoundRect(30, 30, 940, 136, 30, 30);
			if(isAtWorkshop) g.fillRoundRect(990, 30, 346, 136, 30, 30);
			
			g.setFont(new Font("TimesRoman", Font.BOLD, 30));
			g.setColor(new Color(50, 50, 50));
			g.drawString("Inventory", 40, 60);
			if(isAtWorkshop) {
				g.drawString("Crafting", 1000, 60);
				g.drawString("+", 1095, 122);
				g.drawString("=", 1217, 122);
			}
			
			
			g.setColor(new Color(220,220,200, 240));
			for (int i = 0; i < 10; i++) {
				g.setColor(new Color(220,220,200, 240));
				Object invObj = inventory.getObjectAt(i);
				
				if (invObj != null) {
					if (i == inventory.getSelectedIndex()) {
						g.setColor(new Color(180,180,180, 240));
						g.fillRoundRect(45+(i*92), 70, 82, 82, 15, 15);
						g.fillRoundRect(45+(i*92), 152, 122, 82, 15, 15);
						g.setFont(new Font("TimesRoman", Font.BOLD, 20));
						if (invObj.getUsable()) {
							g.setColor(new Color(50, 50, 50));
						} else {
							g.setColor(new Color(120, 120, 120));
						}
						g.drawString("1 - Use", 55+(i*92), 172);g.drawString("1 - Use", 55+(i*92), 172);
						g.setColor(new Color(50, 50, 50));
						g.drawString("2 - Craft", 55+(i*92), 192);
						g.drawString("3 - Cancel", 55+(i*92), 212);
					}  
					else g.fillRoundRect(45+(i*92), 70, 82, 82, 15, 15);
					r.drawImage(invObj.getBigImage(),50+(i*92), 75);
				} else {
					g.fillRoundRect(45+(i*92), 70, 82, 82, 15, 15);
				}
				g.setFont(new Font("TimesRoman", Font.BOLD, 25));
				g.setColor(new Color(50, 50, 50));
				g.drawString(String.valueOf(i+1), 45+(i*92), 100);
			}
			if(isAtWorkshop) {
				for (int i = 0; i < 3; i++) {
					g.fillRoundRect(1000+(i*122), 70, 82, 82, 15, 15);
					if (i<2) {
						if(inventory.getCraftItem(i)!= null) r.drawImage(inventory.getCraftItem(i).getBigImage(),1005+(i*122), 75);
					} else {
						Image imgCraft = (inventory.haveFusion(0) == null) ? null : inventory.haveFusion(0).getBigImage();
						if (imgCraft != null) {
							r.drawImage(imgCraft,1005+(i*122), 75);
							g.setColor(new Color(50, 50, 50));
							g.drawString("Press C", 1150, 60);
						}
					}
				}
			}
			
		} else {
			g.setFont(new Font("TimesRoman", Font.BOLD, 40));
			g.setColor(new Color(50, 50, 50));
			g.drawString("A or Q - inventory", 50, 50);
		}
		
		// Objectives 
		
		if(openObjectives) {
			
			g.setColor(new Color(180, 180, 180, 200));
			g.fillRoundRect(1000,180,330,185, 30, 30);
			g.setColor(new Color(50, 50, 50));
			
			if(gotGlass) {
				g.setFont(new Font("TimesRoman", Font.ITALIC, 25));
				g.drawString("- Take sand with a", 1020, 220);		
				g.drawString("  container and make glass.", 1020, 260);	
			} else {
				g.setFont(new Font("TimesRoman", Font.BOLD, 25));
				g.drawString("- Take sand with a", 1020, 220);		
				g.drawString("  container and make glass.", 1020, 260);					
			}
			
			if(fixDome) {
				g.setFont(new Font("TimesRoman", Font.ITALIC, 25));
				g.drawString("- Fix the dome with glass.", 1020, 300);		
			} else {
				g.setFont(new Font("TimesRoman", Font.BOLD, 25));	
				g.drawString("- Fix the dome with glass.", 1020, 300);					
			}
			
			if(healJam) {
				g.setFont(new Font("TimesRoman", Font.ITALIC, 25));
				g.drawString("- Heal Jam with clay.", 1020, 340);	
			} else {
				g.setFont(new Font("TimesRoman", Font.BOLD, 25));
				g.drawString("- Heal Jam with clay.", 1020, 340);					
			}
		} else {
			g.setFont(new Font("TimesRoman", Font.BOLD, 40));
			g.setColor(new Color(50, 50, 50));
			g.drawString("E - Goals", 1100, 220);			
		}
	}
	
	public static void main(String args[]) {
		GameContainer gc = new GameContainer(new GameManager());
		gc.start();
	}
	
	/* GAME FUNCTIONS */
	
    public boolean canMove(int left, int top, int right,int bot) {
        boolean move = true;
        
        int xIndexStart = (int) Math.floor((player.getPosX()-5)/squareSize);
        int xIndexEnd = xIndexStart + (int) Math.floor(player.getWidth()/squareSize) + 2;
        
        for(int y = 0; y < printMap.length; y ++) {
            for(int x = xIndexStart; x<=xIndexEnd; x++) {
                
                if(map[y][(x >= map[0].length) ? (x-map[0].length) : x].getCollision()) {
                    
                    int[] blockBox = { ((x >= map[0].length) ? (x-map[0].length) : x)*squareSize, (map.length-(y+1)+1)*squareSize,(((x >= map[0].length) ? (x-map[0].length) : x)+1)*squareSize, (map.length-(y+1))*squareSize};
                    
                    int playerLeft = player.getPosX()+left;
                    int playerRight =  player.getPosX()+player.getWidth()+right;
                    
                    if (blockBox[0]+player.getWidth()*2>map[0].length*squareSize) {
                        if (playerRight-player.getWidth()*2<0) {
                            playerRight += map[0].length*squareSize; 
                            playerLeft += map[0].length*squareSize;
                        }                        
                    } else if (blockBox[0]-player.getWidth()*2<0) {
                        if (playerRight+player.getWidth()*2>map[0].length*squareSize) {
                            playerRight -= map[0].length*squareSize; 
                            playerLeft -= map[0].length*squareSize;
                        }
                    }
                    
                    int[] playerBox = {playerLeft,player.getPosY()+player.getHeight()+top,playerRight,player.getPosY()+bot};
                    move = move && !collision(playerBox, blockBox);                    
                }
            }
        }
        return move;
    }
	
	
	public boolean collision (int[] player, int[] block) {	
		if (player[0] < block[2] && player[1] > block[3] && player[2] > block[0] && player[3] < block[1]) {
			return true;
		}
		return false;
	}
	
	public Object popObject() {
        Object returnObject = null;
        for (Iterator<Object> i = objects.iterator(); i.hasNext();) {
            Object item = i.next();
            
            int[] itemBox = { item.getX(), item.getY(), item.getX()+squareSize, item.getY()+squareSize};
            
            int playerLeft = player.getPosX();
            int playerRight =  player.getPosX()+player.getWidth();
            
            if (itemBox[0]+player.getWidth()*2>map[0].length*squareSize) {
                if (playerRight-player.getWidth()*2<0) {
                    playerRight += map[0].length*squareSize; 
                    playerLeft += map[0].length*squareSize;
                }                        
            } else if (itemBox[0]-player.getWidth()*2<0) {
                if (playerRight+player.getWidth()*2>map[0].length*squareSize) {
                    playerRight -= map[0].length*squareSize; 
                    playerLeft -= map[0].length*squareSize;
                }
            }
            
            int[] playerBox = {playerLeft,player.getPosY()+player.getHeight(),playerRight,player.getPosY()};
            if (collision(playerBox, itemBox)) {
                returnObject = item;
                objects.remove(item);
                break;
            }   
        }
        return returnObject;
    }
}