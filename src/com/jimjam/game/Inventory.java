package com.jimjam.game;

import java.util.ArrayList;
import java.util.Arrays;

import com.jimjam.engine.gfx.Image;

public class Inventory {
	
	private boolean open = false;
	private boolean selected;
	private int indexSelected = -1;
	
	private Object[] objects;
	private Object[] crafts;
	
	public Inventory () {
		objects = new Object[10];
		crafts = new Object[2];
		selected = false;
	}
	
	public void init() {
		open = true;
		selected = false;
		indexSelected = -1;
		crafts = new Object[2];
	}
	
	public boolean canAddObject () {
		return Arrays.asList(objects).contains(null);
	}
	
	public void addObject (Object object) {
		for ( int i = 0; i < objects.length; i++) {
			if (objects[i] == null) {
				 objects[i] = object;
				 break;
			}
		}
	}
	
	public Object getObjectAt(int index) {
		return objects[index];
	}
	
	public void dropObject(int index) {
		objects[index] = null;
	}
	
	public boolean getSelected(){
		return selected;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public boolean craftEmpty() {
		return crafts[0] == null || crafts[1] == null;
	}
	
	public void putInCraft(int index) {
		if(crafts[0] == null) {
			crafts[0] = objects[index];
		} else {
			crafts[1] = objects[index];
		}
	}
	
	public void select(int index) {
		selected = true;
		indexSelected = index;
	}
	
	public void unselect() {
		selected = false;
		indexSelected = -1;
	}
	
	public Object haveFusion(int container) { // 0 - Workshop, 1 - Oven
		if(crafts[0] != null || crafts[1] != null) {
			Object tempC1 = crafts[0];
			Object tempC2 = crafts[1];
			if(crafts[0] == null) tempC1 = crafts[1];
			if(crafts[1] == null) tempC2 = crafts[0];
			if(tempC1.getId() == 7 && tempC2.getId() == 7 && container == 0) return new Object(9, "Verre",new Image("/verre.png"),new Image("/verre72.png"),150,200, true);
		}
		
		return null;
	}
	
	public int getSelectedIndex() {
		return indexSelected;
	}
	
	public Object getCraftItem(int index) {
		return crafts[index];
	}
	
	public void deleteCraftItem(int index) {
		if(crafts[0].getId() == index) {
			crafts[0] = crafts[1];
			crafts[1] = null;
		} else if (crafts[1].getId() == index) {
			crafts[1] = null;
		}
	}
	
	public boolean isInCrafts(int index) {
		boolean isIn = false;
		if(crafts[0] != null) isIn = isIn || (crafts[0].getId() == index);
		if(crafts[1] != null) isIn = isIn || (crafts[1].getId() == index);
		return isIn;
	}
	
	public void letsCraft(int container) {
		Object newItem = haveFusion(container);
		for(int i = 0; i<10;i++) {
			if ( objects[i] != null) {
				if(crafts[0] != null) {
					if(objects[i].getId() == crafts[0].getId()) {
						objects[i] = null;
						continue;
					}
				} 
				if (crafts[1] != null){
					if(objects[i].getId() == crafts[1].getId()) {
						objects[i] = null;
						continue;
					}					
				}
			}
		}
		addObject(newItem);
		crafts = new Object[2];
	}
	
	public void useItem(ArrayList<Integer> blocks) {
		for(int i = 0; i<blocks.size();i++) System.out.print(blocks.get(i)+" -- ");
		boolean dropped = false;
		/*if (objects[indexSelected].getId() == 1 && blocks.contains(6)) {
			dropObject(getSelectedIndex());
			selected = false;
			indexSelected = -1;
			addObject(new Object(6, "Water",new Image("/cacahuete_eau.png"),new Image("/cacahuete_eau72.png"),0,0, false));
			dropped = true;
		} 	*/
		if (!dropped && objects[indexSelected].getId() == 1 && (blocks.contains(2) || blocks.contains(3))) {
			dropObject(getSelectedIndex());
			selected = false;
			indexSelected = -1;
			addObject(new Object(7, "Sand",new Image("/cacahuete_sable.png"),new Image("/cacahuete_sable72.png"),0,0, false));
		} 	
	}
}
