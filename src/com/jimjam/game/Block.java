package com.jimjam.game;

import com.jimjam.engine.gfx.Image;

public class Block {
	
	private boolean collision;
	private int image;
	private int type;
	private static Image[] images = new Image[7];
	
	
	public static void init() {
		images[0] = new Image("/pierre_sol.png");
		images[1] = new Image("/pierre_sol_plein.png");
		images[2] = new Image("/sable_sol.png");
		images[3] = new Image("/sable_plein_sol.png");
		images[4] = new Image("/terre_sol.png");
		images[5] = new Image("/terre_plein_sol.png");
		images[6] = new Image("/eau_sol.png");
	}
	
	public Block(int type) {
		this.type = type;
		switch (type) {
			case ' ' : // Void
				image = -1;
				collision = false;
				break;
			case '0' : // rock top
				image = 0;
				collision = true;
				break;
			case '1' : // rock
				image = 1;
				collision = true;
				break;
			case '2' : // Sand top
				image = 2;
				collision = true;
				break;
			case '3' : // Sand
				image = 3;
				collision = true;
				break;
			case '4' : // Grass
				image = 4;
				collision = true;
				break;
			case '5' : // Earth
				image = 5;
				collision = true;
				break;
			case '6' : // Water
				image = 6;
				collision = false;
				break;
		}

	}

	public Image getImage() {
		return (image < 0) ? null : images[this.image];
	}
	
	public boolean getCollision() {
		return collision;
	}

	public int getType() {
		return type;
	}
}

